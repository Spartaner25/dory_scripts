#!/bin/bash
despares=false
system_name=system
if [ -z "$1" ]; then
    echo "Usage: desparse.and.mount.system.sh [-d --despares] path/to/system.img"
    exit 0
fi
if [ "$1" == "-d" ] || [ "$1" == "--despares" ]; then
    despares=true
    shift
fi
if [ -n "$2" ]; then
    system_name=$2
fi
if [ ! -d "out" ]; then
  mkdir out
fi
sudo mkdir out/$system_name
if $despares ; then
    android-simg2img/simg2img $1 out/$system_name.raw.img
    sudo mount -o loop out/$system_name.raw.img out/$system_name
else
    sudo mount -o loop $1 out/$system_name
fi
