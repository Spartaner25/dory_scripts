#!/bin/bash
boot_name=build.boot
base=0x1dfff00
cmdline="androidboot.hardware=dory user_debug=31 maxcpus=4 msm_rtb.filter=0x3F pm_levels.sleep_disabled=1 console=null androidboot.console=null"
if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Usage: build.boot.sh path/to/ramdisk/folder path/to/zImage"
    exit 0
fi
if [ -n "$3" ]; then
    boot_name=$3
fi
./mkbootfs/mkbootfs $1 | gzip > out/$boot_name.gz
./mkbootimg.py --kernel $2 --ramdisk out/$boot_name.gz --base $base --cmdline "$cmdline" -o out/$boot_name.img
rm out/$boot_name.gz
