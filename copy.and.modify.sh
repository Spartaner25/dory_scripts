#!/bin/bash
#This array holds the directorys which should be deleted from the nemo system
array=(
    etc/
    lib/hw/
    media/
    priv-app/CarrierConfig
    priv-app/Lge3DWatchfaceContents
    priv-app/LgeCalendarWatchface
    priv-app/LgeClassicWatchface
    priv-app/LgeClassicWhiteWatchface
    priv-app/LgeHealthWatchface
    priv-app/LgeHikingWatchface
    priv-app/LgeMinimalWatchface
    priv-app/LgeModernWatchface
    priv-app/LgePremiumGoldWatchface
    priv-app/LgeRoundWhiteWatchface
    priv-app/LgeSkyChartWatchface
    priv-app/LgeSteelWatchface
    priv-app/LgeTitanWatchface
    priv-app/LgeVenusWatchface
    priv-app/LgeWeatherWatchface
    priv-app/LgeWorldclockWatchface
    priv-app/LGPulse
    priv-app/LGWHealth
    priv-app/TelephonyProvider
    priv-app/TeleService
    vendor/
)
# This array holds the directorys which should be copied from dory
array2=(
    etc/
    media/
    lib/hw/
    vendor/
)
# This array holds the files which should be copied from dory boot
array3=(
    fstab.dory
    init.dory.rc
    ueventd.dory.rc
)

sudo mkdir out/system.out
#To make sure, that we copy the correct permissions, we copy as sudo
sudo cp -rfp out/system.nemo/* out/system.out/ 
for item in ${array[*]}
do
    sudo rm -r out/system.out/$item
done
for item in ${array2[*]}
do
    sudo cp -rfpv out/system.dory/$item out/system.out/
done
# Remove modem firmware init
sudo rm out/system.out/bin/init.nemo.sh
# Avoid the replacement of the recovery
sudo rm out/system.out/recovery-from-boot.p
sudo rm out/system.out/etc/recovery-resource.dat
sudo rm out/system.out/bin/install-recovery.sh
# Done with the system, onto the boot
mkdir out/out.ramdisk
sudo cp -rfpv out/nemo.ramdisk/* out/out.ramdisk/ 
for item in ${array3[*]}
do
    sudo cp -fpv out/dory.ramdisk/$item out/out.ramdisk/
done
