#!/bin/bash
#This array holds the directorys which should be deleted from the nemo system
array=(
    etc
    lib/hw
    media
    priv-app/HuaweiMember
    priv-app/HwWatchface
    priv-app/HwWatchface3D
    priv-app/HwWearable
    vendor
)
# This array holds the directorys which should be copied from dory
array2=(
    etc
    media
    lib/hw
    vendor
)
# This array holds the files which should be copied from dory boot
array3=(
    fstab.dory
    init.dory.rc
    ueventd.dory.rc
)

sudo mkdir out/system.out
#To make sure, that we copy the correct permissions, we copy as sudo
sudo cp -rfp out/system.nemo/* out/system.out/ 
for item in ${array[*]}
do
    sudo rm -r out/system.out/$item/*
done
for item in ${array2[*]}
do
    sudo cp -rfpv out/system.dory/$item/* out/system.out/$item
done
# Remove modem firmware init
sudo rm out/system.out/bin/delta_panel.sh
# Avoid the replacement of the recovery
sudo rm out/system.out/recovery-from-boot.p
sudo rm out/system.out/etc/recovery-resource.dat
sudo rm out/system.out/bin/install-recovery.sh
sudo cp out/system.dory/build.prop out/system.out/build.prop
# Done with the system, onto the boot
mkdir out/out.ramdisk
sudo cp -rfpv out/nemo.ramdisk/* out/out.ramdisk/ 
for item in ${array3[*]}
do
    sudo cp -fpv out/dory.ramdisk/$item out/out.ramdisk/
done
#sudo cp -fpv default.prop out/out.ramdisk/
