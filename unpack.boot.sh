#!/bin/bash
if [ -z "$1" ]; then
    echo "Usage: unpack.boot.sh path/to/boot.img"
    exit 0
fi
name=boot
if [ -n "$2" ]; then
    name=$2
fi
if [ ! -d "out" ]; then
  mkdir out
fi
./unmkbootimg $1
mv zImage out/$name.zImage
mv initramfs.cpio.gz out/$name.initramfs.cpio.gz
mkdir out/$name.ramdisk
cd out/$name.ramdisk
gunzip -c ../$name.initramfs.cpio.gz | cpio -i
