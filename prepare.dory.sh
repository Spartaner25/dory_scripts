#!/bin/bash
if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Usage: prepare.dory.sh path/to/boot.img path/to/system.img"
    exit 0
fi
if [ ! -d "out" ]; then
  mkdir out
fi
cp $1 out/boot.dory.img
cp $2 out/system.dory.img
# Extract the System to a folder, we need to be sudo for mount
./mount.system.sh out/system.dory.img system.dory
# Split the boot.img and extract the ramdisk to a folder
./unpack.boot.sh out/boot.dory.img dory
