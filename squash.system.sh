#!/bin/bash
squashed_system_name=system.squashed
if [ -z "$1" ]; then
    echo "Usage: squash.system.sh path/to/system/folder"
    exit 0
fi
if [ -n "$2" ]; then
    squashed_system_name=$2
fi
squashfs-tools/squashfs-tools/mksquashfs $1 out/$squashed_system_name.img -comp gzip -b 131072 -no-exports
