#!/bin/bash
#Execute the scripts in order
#Assuming nemo-nvd36i-factory-9cdd2ac0.tgz is in the same folder as this script
#and the dory boot.img and system.img aswell
./prepare.nemo.sh
./prepare.dory.sh boot.img system.img
./copy.and.modify.sh
./squash.system.sh out/system.out system.out
./build.boot.sh out/out.ramdisk out/dory.zImage out.boot
