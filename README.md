## Dory Scripts
> I am not responsible for bricked devices, dead SD cards, thermonuclear war, or you getting fired because the alarm app failed. Please do some research if you have any concerns about features included in the products you find here before flashing it! 
YOU are choosing to make these modifications, and if you point the finger at us for messing up your device, we will laugh at you.
Your warranty will be void if you tamper with any part of your device / software.

Usage:
1. `git clone --recursive https://gitlab.com/Spartaner25/dory_scripts.git`
2. `cd dory_scripts`
3. `make` mkbootfs, squashfs-tools and android-simg2img
4. Place nemo-nvd36i-factory-9cdd2ac0.tgz, dorys system.img and dorys boot.img in the same folder as the scripts
5. `./main.sh`
6. `cd out`
7. `fastboot flash system.out.img` && `fastboot boot out.boot.img`