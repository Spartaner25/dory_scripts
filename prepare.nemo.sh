#!/bin/bash
if [ ! -d "out" ]; then
  mkdir out
fi
tar fvx nemo-nvd36i-factory-9cdd2ac0.tgz -C out/ nemo-nvd36i/image-nemo-nvd36i.zip
unzip out/nemo-nvd36i/image-nemo-nvd36i.zip 'system.img' 'boot.img' -d out/
rm -r out/nemo-nvd36i
mv out/boot.img out/boot.nemo.img
mv out/system.img out/system.nemo.img
# Extract the System to a folder, we need to be sudo for mount
./mount.system.sh -d out/system.nemo.img system.nemo
# Split the boot.img and extract the ramdisk to a folder
./unpack.boot.sh out/boot.nemo.img nemo
